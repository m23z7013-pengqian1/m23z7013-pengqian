[_tb_system_call storage=system/_preview.ks ]

[mask time=10]
[mask_off time=10]
[cm  ]
[bg  storage="43601d87bbca51b75ad5565788e313cfd9e57d57a5e81-rvQ3zb_fw658webp.png"  time="1000"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#
この日青獅子の森にやってきた勇者小隊は、ここから峠区魔王組討伐計画を進めようと、ここで魔王・青獅子と出会う。[p]
[_tb_end_text]

[chara_show  name="勇者队长"  time="1000"  wait="true"  storage="chara/1/勇者队长.png"  width="1328"  height="960"  left="-437"  top="-125"  reflect="false"  ]
[tb_start_text mode=1 ]
#隊長
どうしたの？今、干部クラスの怪物はすべて私のパンチを受けることができますか？[p]
[_tb_end_text]

[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="54"  top="-126"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
だから。と。と。と。と。マジンガー様から頂いた装備を持っているので、致命的なダメージを1度でも防ぐことができます。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#隊長
じゃあ、お前はやっぱり死ぬんだ。俺がここに来たのはたった3つのことだ。討伐、討伐、討伐！チームメイトたち！仕事だ！[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide  name="青狮"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="勇者队长"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="小队成员1"  time="1000"  wait="true"  storage="chara/5/小队成员1.png"  width="1328"  height="960"  left="-432"  top="-141"  reflect="false"  ]
[chara_show  name="小队成员2"  time="1000"  wait="true"  storage="chara/6/小队成员2.png"  width="1328"  height="960"  left="-214"  top="-151"  reflect="false"  ]
[chara_show  name="小队成员3"  time="1000"  wait="true"  storage="chara/7/勇者小队3_.png"  width="1328"  height="960"  left="-26"  top="-163"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#勇者小隊全員
へへへ、もう待ちきれない！[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide  name="小队成员1"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="小队成员2"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="小队成员3"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="10"  top="-111"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#青獅子
怪物がある怪物がある怪物がある、他の怪物の！私はあなた達のために変な時間を節約することができます。[p]
[_tb_end_text]

[chara_show  name="勇者队长"  time="1000"  wait="false"  storage="chara/1/勇者队长.png"  width="1328"  height="960"  left="-448"  top="-114"  reflect="false"  ]
[tb_start_text mode=1 ]
#隊長
え？ちょっと面白いですが、詳しく話してください。[p]
[_tb_end_text]

[chara_hide  name="青狮"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="小白"  time="1000"  wait="true"  storage="chara/8/小白.png"  width="1328"  height="960"  left="-10"  top="-173"  reflect="false"  ]
[tb_start_text mode=1 ]
#小兵
本当に心が冷たくて、私達の隊長は善意であなた達を助けて、内部から魔王城を瓦解して、あなた達は意外にも上がってきて人を殴る！！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#隊長
彼の言うことを聞いて、あなたはやはり良い怪物ですか？それとも怪物を殴るのを手伝ってくれるいい怪物？[p]
[_tb_end_text]

[chara_hide  name="勇者队长"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="-447"  top="-106"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
すごいね。白は大丈夫だよ、ただの致命傷だよ。[p]
たった2日寝ているだけで死ぬんだよ。[p]
大したことはありません、今最も重要なのは。[p]
魔王城の他の場所の様子を勇者たちに伝え、暗部へ連れて行け！！[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide  name="小白"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="青狮"  time="1000"  wait="true"  pos_mode="true"  ]
[cm  ]
[bg  time="1000"  method="crossfade"  storage="7eccc3608a0193f657cfc8733e722fbc70b3c04eac44-vdufXy_fw658webp.png"  ]
[chara_show  name="小白"  time="1000"  wait="true"  storage="chara/8/小白.png"  width="1328"  height="960"  left="31"  top="-142"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#小兵
幸い隊長が大丈夫だと言ってくれてよかった、そうでなければ私はあなた達を相手にしたくない！[p]
この魔王城の怪物分布図、でも隊長は毎日毎晩、7年も整理し続けてやっと描けたのです！まさに精良中の精良、機密中の機密！[p]
私たちがこの7年間どのように過ごしてきたか知っていますか。目を見開いてよく見なければならない！！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#
如何三句話勇者小隊の装備をすべてだまして人間王国を布石し、ロマンス詐欺を展開する。[p]
[_tb_end_text]

[chara_show  name="小队成员1"  time="1000"  wait="true"  storage="chara/5/小队成员1.png"  width="1328"  height="960"  left="-449"  top="-111"  reflect="false"  ]
[tb_start_text mode=1 ]
#戦士
隊長、どうやらあなたは怪物を打ちました、甚だしきに至っては私達は1人で更に1拳を打つべきです！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#小兵
君たちは何も見ていない！！[p]
[_tb_end_text]

[chara_hide  name="小队成员1"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="小白"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="11"  top="-133"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
あはは、さっきのは一つの模範を作って、主には王国の展開する防詐反詐の活働に応じて、へへへへ[p]
[_tb_end_text]

[chara_show  name="勇者队长"  time="1000"  wait="true"  storage="chara/1/勇者队长.png"  width="1328"  height="960"  left="-405"  top="-122"  reflect="false"  ]
[tb_start_text mode=1 ]
#隊長
私はどうしてまだちょっと信じられないの？[p]
[_tb_end_text]

[chara_show  name="小队成员1"  time="1000"  wait="true"  storage="chara/5/小队成员1.png"  width="1328"  height="960"  left="-552"  top="-152"  reflect="false"  ]
[tb_start_text mode=1 ]
#戦士
確かに[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#青獅子
勇者様、二人だけで話をしましょう。[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide  name="青狮"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="勇者队长"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="小队成员1"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="e031b6b731fc7a874cec14c082307622a1571c313ef90-VB5Exx.jpg"  ]
