[_tb_system_call storage=system/_scene1.ks]

[cm  ]
[bg  storage="43601d87bbca51b75ad5565788e313cfd9e57d57a5e81-rvQ3zb_fw658webp.png"  time="1000"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#
この日青獅子の森にやってきた勇者小隊は、ここから峠区魔王組討伐計画を進めようと、ここで魔王・青獅子と出会う。[p]
[_tb_end_text]

[chara_show  name="勇者队长"  time="1000"  wait="true"  storage="chara/1/勇者队长.png"  width="1328"  height="960"  left="-437"  top="-125"  reflect="false"  ]
[tb_start_text mode=1 ]
#隊長
どうしたの？今、干部クラスの怪物はすべて私のパンチを受けることができますか？[p]
[_tb_end_text]

[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="54"  top="-126"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
だから。と。と。と。と。マジンガー様から頂いた装備を持っているので、致命的なダメージを1度でも防ぐことができます。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#隊長
じゃあ、お前はやっぱり死ぬんだ。俺がここに来たのはたった3つのことだ。討伐、討伐、討伐！チームメイトたち！仕事だ！[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide  name="青狮"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="勇者队长"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="小队成员1"  time="1000"  wait="true"  storage="chara/5/小队成员1.png"  width="1328"  height="960"  left="-432"  top="-141"  reflect="false"  ]
[chara_show  name="小队成员2"  time="1000"  wait="true"  storage="chara/6/小队成员2.png"  width="1328"  height="960"  left="-214"  top="-151"  reflect="false"  ]
[chara_show  name="小队成员3"  time="1000"  wait="true"  storage="chara/7/勇者小队3_.png"  width="1328"  height="960"  left="-26"  top="-163"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#勇者小隊全員
へへへ、もう待ちきれない！[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide  name="小队成员1"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="小队成员2"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="小队成员3"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="10"  top="-111"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#青獅子
怪物がある怪物がある怪物がある、他の怪物の！私はあなた達のために変な時間を節約することができます。[p]
[_tb_end_text]

[chara_show  name="勇者队长"  time="1000"  wait="false"  storage="chara/1/勇者队长.png"  width="1328"  height="960"  left="-448"  top="-114"  reflect="false"  ]
[tb_start_text mode=1 ]
#隊長
え？ちょっと面白いですが、詳しく話してください。[p]
[_tb_end_text]

[chara_hide  name="青狮"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="小白"  time="1000"  wait="true"  storage="chara/8/小白.png"  width="1328"  height="960"  left="-10"  top="-173"  reflect="false"  ]
[tb_start_text mode=1 ]
#小兵
本当に心が冷たくて、私達の隊長は善意であなた達を助けて、内部から魔王城を瓦解して、あなた達は意外にも上がってきて人を殴る！！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#隊長
彼の言うことを聞いて、あなたはやはり良い怪物ですか？それとも怪物を殴るのを手伝ってくれるいい怪物？[p]
[_tb_end_text]

[chara_hide  name="勇者队长"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="-447"  top="-106"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
すごいね。白は大丈夫だよ、ただの致命傷だよ。[p]
たった2日寝ているだけで死ぬんだよ。[p]
大したことはありません、今最も重要なのは。[p]
魔王城の他の場所の様子を勇者たちに伝え、暗部へ連れて行け！！[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide_all  time="1000"  wait="true"  ]
[cm  ]
[bg  time="1000"  method="crossfade"  storage="7eccc3608a0193f657cfc8733e722fbc70b3c04eac44-vdufXy_fw658webp.png"  ]
[chara_show  name="小白"  time="1000"  wait="true"  storage="chara/8/小白.png"  width="1328"  height="960"  left="31"  top="-142"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#小兵
幸い隊長が大丈夫だと言ってくれてよかった、そうでなければ私はあなた達を相手にしたくない！[p]
この魔王城の怪物分布図、でも隊長は毎日毎晩、7年も整理し続けてやっと描けたのです！まさに精良中の精良、機密中の機密！[p]
私たちがこの7年間どのように過ごしてきたか知っていますか。目を見開いてよく見なければならない！！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#
如何三句話勇者小隊の装備をすべてだまして人間王国を布石し、ロマンス詐欺を展開する。[p]
[_tb_end_text]

[chara_show  name="小队成员1"  time="1000"  wait="true"  storage="chara/5/小队成员1.png"  width="1328"  height="960"  left="-449"  top="-111"  reflect="false"  ]
[tb_start_text mode=1 ]
#戦士
隊長、どうやらあなたは怪物を打ちました、甚だしきに至っては私達は1人で更に1拳を打つべきです！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#小兵
君たちは何も見ていない！！[p]
[_tb_end_text]

[chara_hide  name="小队成员1"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="小白"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="11"  top="-133"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
あはは、さっきのは一つの模範を作って、主には王国の展開する防詐反詐の活働に応じて、へへへへ[p]
[_tb_end_text]

[chara_show  name="勇者队长"  time="1000"  wait="true"  storage="chara/1/勇者队长.png"  width="1328"  height="960"  left="-405"  top="-122"  reflect="false"  ]
[tb_start_text mode=1 ]
#隊長
私はどうしてまだちょっと信じられないの？[p]
[_tb_end_text]

[chara_show  name="小队成员1"  time="1000"  wait="true"  storage="chara/5/小队成员1.png"  width="1328"  height="960"  left="-552"  top="-152"  reflect="false"  ]
[tb_start_text mode=1 ]
#戦士
確かに[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#青獅子
勇者様、二人だけで話をしましょう。[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide_all  time="1000"  wait="true"  ]
[bg  time="1000"  method="crossfade"  storage="e031b6b731fc7a874cec14c082307622a1571c313ef90-VB5Exx.jpg"  ]
[chara_show  name="青狮"  time="1000"  wait="true"  left="-7"  top="-52"  width=""  height=""  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#青獅子
勇者様、友達になって、助けてあげましょう、本気です、黒鳥と白象の二人の弱点、弱点、弱点、全部教えてあげます！[p]
[_tb_end_text]

[chara_show  name="勇者队长"  time="1000"  wait="true"  left="-460"  top="-32"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#隊長
私たちには勝てないと思う？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#青獅子
そういう意味じゃない！[p]
あなたも知っているでしょう~彼らの2つの背景は深くて、吐き気が終わってあなた達はまたおとなしく帰って出勤することができます！[p]
私は彼らを知っていて、彼らの武器、秘密兵器！お宝の隠れ家も！これで彼らは気持ち悪くなることができます！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#隊長
でも、私はあなたの言葉を感じて、私はどうしてあまり信じませんか？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#青獅子
どうしてですか[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#
勇者隊長が壁に目を向けたツーショット写真[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#隊長
私はあなたたち3人は生命の危険を冒した良い兄弟のような気がするから。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#青獅子
これはね、それはもう何年も前のことだよ。どんなによかったことか、今からどんなに悪いことか。[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[tb_start_text mode=1 ]
#
青獅子は思い出にはまった[p]
[_tb_end_text]

[tb_hide_message_window  ]
[bg  time="1000"  method="crossfade"  storage="7eccc3608a0193f657cfc8733e722fbc70b3c04eac44-vdufXy_fw658webp.png"  ]
[chara_show  name="黑鸟"  time="1000"  wait="true"  storage="chara/4/黑鸟.png"  width="1328"  height="960"  left="-479"  top="-85"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#黒鳥
こんにちは、自己紹介して、私の名前は黒鳥、西方の魔神は私の叔父で、戦闘科の出身で、戦闘力は私たちの中で最も強いべきで、後でいじめられた私を探しています。[p]
[_tb_end_text]

[chara_show  name="白象"  time="1000"  wait="true"  storage="chara/3/白象.png"  width="1328"  height="960"  left="-231"  top="-79"  reflect="false"  ]
[tb_start_text mode=1 ]
#白象
私は北方魔神の配下の防御課の魔王、今出向されてきたので、私は白象と呼んで、[p]
私は私達の中で最もお金があるべきで、北方魔王は私がこちらで餓えてやせたことを恐れて、[p]
だから私に1枚の限りなく貸し越しのクレジットカードをあげて、後でどんな消費が私の身に包みます！[p]
[_tb_end_text]

[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="49"  top="-77"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
私は南方の魔王の下の隊長で、本土の怪物で、青獅子と呼ばれて、何の特技がありませんが、しかしあなた達2人と私は1つの寮でさえすれば、衛生と食事を私はすべて包むことができました！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#白象と黒鳥
義父！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#黒鳥
今日から、私たち三人は良い兄弟になって、この山嶺区魔王城は私たちの出発点です！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#白象
課員から隊長まで、どんな職位にいても、私たちはすべて精進しなければなりません！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#青獅子
我々3人はこの山嶺区の魔王城で名鎮側の魔王になるんだ！エリート中のエリートになれ！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#黒鳥、青獅子、白象
ハハハハハハ！！[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[tb_hide_message_window  ]
[bg  time="1000"  method="crossfade"  storage="e031b6b731fc7a874cec14c082307622a1571c313ef90-VB5Exx.jpg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#
青獅子の思い出が終わる[p]
[_tb_end_text]

[chara_show  name="勇者队长"  time="1000"  wait="true"  storage="chara/1/勇者队长.png"  width="1328"  height="960"  left="-437"  top="-34"  reflect="false"  ]
[tb_start_text mode=1 ]
#隊長
だからお前は兄貴になって、白象は二弟になって、逆に背景が一番牛で、戦力が一番高い黒鳥が三男になったんだ。[p]
[_tb_end_text]

[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="10"  top="-56"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
へへ、当初の小細工は、何の値打ちもない。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#隊長
じゃ、どうして私が来た時と違うの？これが3つの地域になってしまいました。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#青獅子
ある怪物は苦しみを共にすることができて、甘美を共にすることができない。[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[tb_start_text mode=1 ]
#
青獅子は再び思い出にふけった[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="43601d87bbca51b75ad5565788e313cfd9e57d57a5e81-rvQ3zb_fw658webp.png"  ]
[tb_start_text mode=1 ]
#ナレーション
社員を勧誘するために、青獅子、黒鳥、白象があちこちに求人広告を出しています。[p]
[_tb_end_text]

[chara_show  name="白象"  time="1000"  wait="true"  storage="chara/3/白象.png"  width="1328"  height="960"  left="37"  top="-44"  reflect="false"  ]
[tb_start_text mode=1 ]
#白象
私たちのこの手は効果がありますか？何日も貼っていて、面接の弟は一人もいません。このままだと、いつになったら俺たちは大隊長になれるんだろう。[p]
[_tb_end_text]

[chara_show  name="黑鸟"  time="1000"  wait="true"  storage="chara/4/黑鸟.png"  width="1328"  height="960"  left="-481"  top="-80"  reflect="false"  ]
[tb_start_text mode=1 ]
#黒鳥
さもないとどうしますか。一番上の兄は今は壁を見ることができないと言って、見ると張り巡らされているようだ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#
画面は求人広告に移って、上には次のように書かれている：山嶺区魔王城求人広告、銃を手にして、私と一緒に来て、[p]
私たち山嶺区魔王城納新、五険一金、食うか食うか、住むか、相手を分配するか、チャンスを逃すことはできません、この広告を過ぎると、二度とチャンスはありません。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#黒鳥
今はぼんやりしています。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#白象
ガールフレンドを追いかけるときも、そんなに力を入れなかったんだって。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#黒鳥
さすがお兄ちゃんですね。彼はきっと成功すると思う。[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[tb_start_text mode=1 ]
#
画面が一転[p]
[_tb_end_text]

[chara_show  name="青狮"  time="1000"  wait="true"  storage="chara/2/青狮.png"  width="1328"  height="960"  left="-428"  top="-78"  reflect="false"  ]
[tb_start_text mode=1 ]
#青獅子
銃を手にして、私と一緒に来て、私達の山嶺区魔王城納新！ 五険一金、食うも食わぬも食わぬ！パッケージは対象を分けますね。！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#みんなの弟
はい、お兄さん！！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#青獅子
でも覚えておいてください。私たちのギャングは福祉がいいですが、規則も多いです。[p]
あなた方の以前の焼き殺し略奪の習慣を片付けて、人がそれを食べるのですか？それは下品な怪物だからこそできることだ。[p]
私たちは彼らとビジネスをして、平和に付き合わなければならない、知っていますか！[p]

[_tb_end_text]

